<?php

use Illuminate\Http\Request;
use App\Http\Controllers\Api\V1\{
    UsersController,
    DepositsController,
    WithdrawalsController,
    TransactionsController,
    OrdersController,
    TradesController
};

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('api_key')->prefix('v1')->group(function() {
    Route::get('/users/search', [UsersController::class, 'search']);
    Route::put('/users/{user}', [UsersController::class, 'update']);

    // Deposits
    Route::get('/deposits', [DepositsController::class, 'index']);

    // Transactions
    Route::get('/transactions', [TransactionsController::class, 'index']);
    Route::post('/transactions/{type}', [TransactionsController::class, 'storeTicket'])
        ->where(['type' => 'bonus|penalty']);

    // Withdrawals
    Route::get('/withdrawals', [WithdrawalsController::class, 'index']);

    // Orders
    Route::get('/orders', [OrdersController::class, 'index']);

    // Trades
    Route::get('/trades', [TradesController::class, 'index']);
});
//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
