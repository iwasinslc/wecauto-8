{{ $plan['name'] }} - {{ $plan['currency']['code'] }}
{{ __('Minimum investment') }}: {{ $plan['min'] }}, {{ __('Maximum investment') }}
: {{ $plan['max'] }}, {{ __('Daily interest') }}: {{ $plan['daily'] }}
%, {{ __('Plan duration') }}: {{ $plan['duration'] }}
