<?php
$summary = 0;

?>
@if (is_array($levels) && count($levels) > 0)
@foreach($levels as $k => $v)
{{__('Level')}}: <b>{{ $k }}</b> - {{__('Referrals')}}: <b>{{ $v }}</b>
<?php $summary += $v; ?>
@endforeach

@endif
{{__('Total')}} : {{$summary}}