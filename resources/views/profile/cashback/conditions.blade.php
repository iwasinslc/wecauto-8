@extends('layouts.profile')
@section('title', 'Как получить Cashback?')

@section('content')
    <div class="cashback">
        <!---->
        <section>
            <div class="container">
                <ul class="tabs-navigation">
                    <li class="is-active"><a href="{{ route('profile.cashback.conditions') }}">Как получить Cash Back?</a>
                    </li>
                    <li><a href="{{ route('profile.cashback.create') }}">Подать заявку</a>
                    </li>
                    <li><a href="{{ route('profile.cashback.index') }}">Мои заявки</a>
                    </li>
                </ul>
            </div>
        </section>
        <!---->
        <section class="cashback-info">
            <div class="container">
                <div class="cashback-info__row">
                    <div class="cashback-info__col">
                        <h3 class="lk-title">Получение Cash Back
                        </h3>
                        <ol class="cashback-steps">
                            <li>
                                <div class="cashback-steps__text">
                                    <p>Купить авто/мототранспорт в автосалоне и снять видеоролик (технические требования к ролику указаны ниже)</p>
                                </div>
                            </li>
                            <li>
                                <div class="cashback-steps__text">
                                    <p>Зарегистрировать авто/мото ТС в соответствующих регистрационных органах и получить свидетельство о регистрации транспортного средства (СОР) или иной регистрационный документ</p>
                                </div>
                            </li>
                            <li>
                                <div class="cashback-steps__text">
                                    <p>Подать заявку в компанию WEB TOKEN PROFIT и представить документы, указанные ниже</p>
                                </div>
                            </li>
                            <li>
                                <div class="cashback-steps__text">
                                    <p>Подача заявки: подача заявки осуществляется путем подачи копий документов на транспортное средство и видеоролика согласно требованиям, указанным ниже</p><a class="btn btn--warning" href="{{ route('profile.cashback.create') }}">Подать заявку</a>
                                </div>
                            </li>
                        </ol>
                    </div>
                    <div class="cashback-info__col">
                        <h3 class="lk-title">Требования
                        </h3>
                        <div class="primary-block">
                            <div class="primary-block__icon"><img src="/assets/images/cashback/key.svg" alt="">
                            </div>
                            <div class="primary-block__description">
                                <p>Авто/мототранспорт (новый или б/у не старше 3-х лет) должен быть приобретен исключительно в автосалоне</p>
                            </div>
                        </div>
                        <div class="primary-block primary-block--gradient-inverse">
                            <div class="primary-block__icon"><img src="/assets/images/cashback/triangle.svg" alt="">
                            </div>
                            <div class="primary-block__description">
                                <p>Авто/мототранспорт не должен иметь внешних механических повреждений (вмятин, царапин, разбитых стёкол, разбитых зеркал заднего вида и т.д.)</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!---->
        <section class="requirements-video">
            <div class="container">
                <h3 class="lk-title">Требования к видеороликам
                </h3>
                <div class="requirements-video__block">
                    <div class="requirements-video__row">
                        <div class="requirements-video__col">
                            <div class="requirements-video__item">
                                <div class="requirements-video__icon"><img src="/assets/images/cashback/1.svg" alt="">
                                </div>
                                <div class="requirements-video__description">
                                    <p>Профессиональный ролик получения ТС в автосалоне</p>
                                </div>
                            </div>
                            <div class="requirements-video__item">
                                <div class="requirements-video__icon"><img src="/assets/images/cashback/4.svg" alt="">
                                </div>
                                <div class="requirements-video__description">
                                    <p>История вашего успеха в компании WEB TOKEN PROFIT (необходимо представиться, рассказать о том, что ТС приобретено по программе WecAuto сообщества Web Token Profit, а также мотивировать зрителя участвовать в данной программе)</p>
                                </div>
                            </div>
                        </div>
                        <div class="requirements-video__col">
                            <div class="requirements-video__item">
                                <div class="requirements-video__icon"><img src="/assets/images/cashback/2.svg" alt="">
                                </div>
                                <div class="requirements-video__description">
                                    <p>Высокое разрешение видео от 1080p – 3840</p>
                                </div>
                            </div>
                            <div class="requirements-video__item">
                                <div class="requirements-video__icon"><img src="/assets/images/cashback/5.svg" alt="">
                                </div>
                                <div class="requirements-video__description">
                                    <p>Длительность ролика 5-10 мин</p>
                                </div>
                            </div>
                        </div>
                        <div class="requirements-video__col">
                            <div class="requirements-video__item">
                                <div class="requirements-video__icon"><img src="/assets/images/cashback/3.svg" alt="">
                                </div>
                                <div class="requirements-video__description">
                                    <p>Фирменная атрибутика компании WEB TOKEN PROFIT (логотип на автомобиле), макеты логотипов в различных вариантах необходимо скачать в кабинете. Поклейка должна быть качественной и профессиональной (пригласите специалиста из рекламного отдела в автосалон для поклейки на авто)</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!---->
        <section class="cashback-description">
            <div class="container">
                <div class="requirements">
                    <div class="requirements__section">
                        <h5 class="requirements__title">Минимальные технические требования:
                        </h5>
                        <ul>
                            <li>1920 x 1080 - 2k Full HD разрешение;</li>
                            <li>.mov/.mp4 – формат;</li>
                            <li>25 FPS - частота кадров в секунду.</li>
                        </ul>
                    </div>
                    <div class="requirements__section">
                        <h5 class="requirements__title">Оптимальные технические требования:
                        </h5>
                        <ul>
                            <li>3840 x 2160 - 4k UHD разрешение;</li>
                            <li>.avi – формат;</li>
                            <li>25 FPS - частота кадров в секунду.</li>
                        </ul>
                    </div>
                </div>
                <h3 class="lk-title">При создании ролика учитывайте определенные нюансы для оптимизации качества видеоматериала:
                </h3>
                <div class="typography">
                    <ul class="list-accent-line">
                        <li>Человек в кадре должен находиться в фокусе;</li>
                        <li>Движения камеры ровные и плавные;</li>
                        <li>Съёмку при помощи камеры производить с одетым внатяжку ремнём или с использованием штатива для более плавных движений и/или статичности камеры;</li>
                        <li>Съёмку видео производить ТОЛЬКО в горизонтальном положении камеры;</li>
                        <li>При съемке автомобиля и человека одновременно оба объекта должны занимать 2/3 пространства по высоте, также выдерживайте небольшой отступ сбоку от края кадра до человека;</li>
                        <li>При съемке только одного человека он должен находиться по центру, по бокам и сверху делаем отступ.</li>
                    </ul>
                </div>
            </div>
        </section>
        <!---->
        <section>
            <div class="container">
                <h3 class="lk-title">Документы, прилагаемые к заявке:
                </h3>
                <div class="typography">
                    <ul class="list-accent-line">
                        <li>Четкое изображение договора купли-продажи нового автомобиля (всех страниц договора-купли продажи);</li>
                        <li>Четкое изображение (скриншот, pdf-файл и т.д.) свидетельства о регистрации (СОР) либо иного регистрационного документа на транспортное средство с обеих сторон, с информацией о технических характеристиках ТС и о его собственнике;</li>
                        <li>Логин и e-mail в проекте.</li>
                    </ul>
                </div>
                <div class="logo-pack"><a class="logo-pack__button" href="/logo.zip" download="download"> <span>Скачать <strong>logo pack</strong></span></a>
                    <div class="logo-pack__image"><img src="/assets/images/cashback/auto.png" alt="">
                    </div>
                </div>
            </div>
        </section>
        <!---->
        <section>
            <div class="container">
                <div class="row-40">
                    <div class="row-40__col-6">
                        <div class="cash-info-block">
                            <h4 class="cash-info-block__title">Размер и актив получения CASH BACK
                            </h4>
                            <div class="cash-info-block__icon"><img src="/assets/images/cashback/icon1.png" alt="">
                            </div>
                            <div class="cash-info-block__description">
                                <p>Кешбэк составляет 9% от стоимости автомобиля, указанной в договоре купли-продажи транспортного средства, но не более 30% от первоначального взноса, которую пользователь внес на сайте <a href="https://wecauto.com" target="_blank">https://wecauto.com</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="row-40__col-6">
                        <div class="cash-info-block cash-info-block--primary">
                            <h4 class="cash-info-block__title">Сроки получения<br> CASH BACK
                            </h4>
                            <div class="cash-info-block__icon"><img src="/assets/images/cashback/icon2.png" alt="">
                            </div>
                            <div class="cash-info-block__description">
                                <p>Перевод CASH BACK осуществляется на баланс личного кабинета партнера на сайте <a href="https://wecauto.com" target="_blank">https://wecauto.com</a> в течение 24 часов после получение полного комплекта документов, его аудита и оценки видеоролика о покупке автомобиля в салоне.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!---->
        <section>
            <div class="container">
                <h3 class="lk-title">Регламент ответа по заявке:
                </h3>
                <div class="typography">
                    <ol class="numbers-list">
                        <li>Рассмотрение заявки и принятие решения осуществляется до 5 суток со дня предоставления полного комплекта документов;</li>
                        <li>Администрация компании WEB TOKEN PROFIT вправе потребовать иные документы для принятия решения по заявке.</li>
                    </ol>
                </div>
            </div>
        </section>
    </div>
@endsection