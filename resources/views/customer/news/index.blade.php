@extends('layouts.auth')
@section('title', __('News'))

@section('content')

    <section class="page-preview">
        <div class="container">
            <div class="breadcrumbs"><a href="{{route('customer.main')}}">{{__('Home')}}</a><span>/</span><span>{{__('News')}}</span>
            </div>
            <h1 class="page-preview__title"><span class="color-primary">{{__('News')}}</span>
            </h1>
        </div>
    </section>
    <section class="news">
        <div class="container">
            <div class="news__row">
            @forelse($news as $new)
                <div class="news__col"><a class="news-item js-animation" href="{{route('customer.news.item', ['slug'=>$new->parent->slug])}}" data-emergence="hidden"><span class="news-item__image"><img src="{{$new->getImgPath()}}" alt=""></span><span class="news-item__date">{{\Carbon\Carbon::parse($new->created_at)->format('d/m/Y')}}</span>
                        <p class="news-item__title">{!! $new->title !!}
                        </p>
                        <p class="news-item__desc">{!! $new->teaser !!}
                        </p><span class="news-item__arrow">
                      <svg class="svg-icon">
                        <use href="/assets/icons/sprite.svg#icon-right-arrow"></use>
                      </svg></span></a>
                </div>
                @empty
                    <div>
                        No news yet
                    </div>
            @endforelse
            </div>
            <div class="news__bottom">
                {{$news->links('partials.pagi') }}
            </div>
        </div>
    </section>


@endsection

