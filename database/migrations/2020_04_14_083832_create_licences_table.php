<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLicencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('licences', function (Blueprint $table) {
            $table->increments('id');
            $table->float('price')->default(0);
            $table->integer('duration')->default(0);
            $table->integer('levels')->default(0);
            $table->float('buy_amount')->default(0);
            $table->float('sell_amount')->default(0);
            $table->float('deposit_min')->default(0);
            $table->float('deposit_max')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('licences');
    }
}
