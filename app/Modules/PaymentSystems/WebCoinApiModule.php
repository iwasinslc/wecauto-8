<?php
namespace App\Modules\PaymentSystems;

use App\Models\Currency;
use App\Models\PaymentSystem;
use App\Models\Transaction;
use App\Models\Wallet;
use App\Models\User;
use App\Models\Withdraw;


/**
 * Class EtherApiModule
 * @package App\Modules\PaymentSystems
 */
class WebCoinApiModule
{
//    /**
//     * @return array
//     * @throws \Exception
//     */
    public static function getBalances(): array
    {
        if(config('app.env') == 'develop') {
            return [];
        }
        $ps       = PaymentSystem::getByCode('webcoinapi');
        $balances = [];

        foreach ($ps->currencies as $currency) {
            try {
                $balances[$currency->code] = self::getBalance($currency->code);
            } catch (\Exception $exception) {
                throw new \Exception($exception->getMessage());
            }
        }

        if (count($balances) > 0 && !empty($ps)) {
            $ps->update([
                'external_balances' => json_encode($balances),
                'connected' => true,
            ]);
        } else {
            $ps->update([
                'external_balances' => json_encode([]),
                'connected' => false,
            ]);
            throw new \Exception('Balance is not reachable.');
        }

        return $balances;
    }
    
    public static function transfer(Withdraw $transaction)
    {

        if(config('app.env') == 'develop') {
            return substr(preg_replace('/[^0-9]/', '', $transaction->id), 0, 6);;
        }

        /** @var Wallet $wallet */
        $wallet         = $transaction->wallet()->first();
        /** @var User $user */
        $user           = $wallet->user()->first();
        /** @var PaymentSystem $paymentSystem */
        $paymentSystem  = $wallet->paymentSystem()->first();
        /** @var Currency $currency */
        $currency       = $wallet->currency()->first();

        if (null === $wallet || null === $user || null === $paymentSystem || null === $currency) {
            throw new \Exception('Wallet, user, payment system or currency is not found for withdrawal approve.');
        }

        $data = [
            'address'=>$transaction->source,
            'amount'=>$transaction->amount-$transaction->commission,
            'tag'=>substr(preg_replace('/[^0-9]/', '', $transaction->id), 0, 6)
        ];

        $result = self::request('GET','send', $data);

        if (isset($result->error)) {
            throw new \Exception('Can not withdraw '.$transaction->amount.$currency->symbol.'. Reason: '.$result->error);
        }



        return substr(preg_replace('/[^0-9]/', '', $transaction->id), 0, 6);;
    }

    /**
     * @param string $currency
     * @return float
     * @throws \Exception
     */
    public static function getBalance(string $currency)
    {
        $currency = Currency::where('code', $currency)->first();

        $result = self::request('GET','balance');



        return $result->result;
    }

    /**
     * @param Transaction $transaction
     * @return mixed
     * @throws \Exception
     */
    public static function createTopupTransaction(Transaction $transaction)
    {



        $req = [
            'amount'      => $transaction->amount,
            'address'     => env('WEBCOIN_API_ADDRESS'), // leave blank send to follow your settings on the Coin Settings page
            'tag'     => $transaction->id,
        ];


        $result = self::request('GET','track', $req);

        if (isset($result->error) ) {
            throw new \Exception($result->error);
        }

        return $result->result;
    }

    /**
     * @param string $currency
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */

    public static function getAddress()
    {

        $result = self::request('GET','give');


        \Log::critical(print_r($result, true));

        return $result->result;
    }


    /**
     * @param string $method
     * @param string $address
     * @param array $data
     * @param array|null $additionHeaders
     * @return object
     * @throws \Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function request($method, $address, $data=[], $additionHeaders=null)
    {
        $client   = new \GuzzleHttp\Client();
        $headers  = [
            'Accept'        => 'application/json',
            'Content-Type'  => 'application/json',
        ];
        $params   = [
            'headers' => array_merge($headers, (is_array($additionHeaders) ? $additionHeaders : [])),
            'verify'  => false,
            //'json'    => $data,
        ];

        $fields = http_build_query($data, '', '&');

        try {
            $response = $client->request($method, 'http://wec.etherapi.net/api/v2/.'.$address.'?key='.env('WEBCOIN_API_KEY').(!empty($data) ? '&'.$fields : ''), $params);
        } catch (\Exception $e) {
            throw new \Exception('Request to '.$address.' is failed. '.$e->getMessage());
        }

        if ($response->getStatusCode() !== 200) {
            throw new \Exception('Request to '.$address.' was with response status '.$response->getStatusCode());
        }

        return json_decode($response->getBody()->getContents());
    }
}