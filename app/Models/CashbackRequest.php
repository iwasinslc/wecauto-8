<?php

namespace App\Models;

use App\Traits\ModelTrait;
use App\Traits\Uuids;
use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

/**
 * Class CashbackRequest
 * @package App\Models
 * @property string $id
 * @property string $user_id
 * @property string $telegram
 * @property boolean $documents_checked
 * @property string $video_link
 * @property boolean $video_checked
 * @property string $comment
 * @property float $amount Cashback amount
 * @property string $result
 * @property boolean $approved
 */
class CashbackRequest extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;
    use Uuids;
    use ModelTrait;

    protected $keyType = 'string';
    public $incrementing = false;

    protected $fillable = [
        'user_id',
        'telegram',
        'documents_checked',
        'video_link',
        'video_checked',
        'comment',
        'amount',
        'result',
        'approved',
    ];

    public function documents()
    {
        return $this->morphMany(Media::class, 'model');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * Approve cashback request
     * @param $amount
     * @param $result
     * @throws \Throwable
     */
    public function approve($amount, $result)
    {
        $currency = Currency::getByCode('FST');

        /** @var Wallet $wallet */
        $wallet = Wallet::query()
            ->where('user_id', $this->user_id)
            ->where('currency_id', $currency->id)
            ->lockForUpdate()
            ->first();

        \DB::transaction(function () use ($wallet, $amount, $result) {
            Transaction::cashback($wallet, $amount, $this->id);

            $wallet->addAmountWithoutAccrueToPartner($amount);

            $this->update([
                'amount' => $amount,
                'result' => $result,
                'video_checked' => true,
                'documents_checked' => true,
                'approved' => true
            ]);
        });
    }
}
