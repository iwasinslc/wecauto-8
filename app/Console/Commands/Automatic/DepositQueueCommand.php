<?php
namespace App\Console\Commands\Automatic;

use App\Jobs\AccrueDeposit;
use App\Jobs\CloseDeposit;
use App\Models\Deposit;
use App\Models\DepositQueue;
use Illuminate\Console\Command;

/**
 * Class DepositQueueCommand
 * @package App\Console\Commands\Automatic
 */
class DepositQueueCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deposits:queue';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run deposits queues.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return void
     */
    public function handle()
    {


        $deposits = Deposit::where('active', true)->where('datetime_closing', '<=', now()->toDateTimeString())
            ->get();

        foreach ($deposits as $deposit) {
            /**
             * @var Deposit $deposit
             */

            $deposit->close();
        }

    }
}
