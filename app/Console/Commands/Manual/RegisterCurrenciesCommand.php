<?php
namespace App\Console\Commands\Manual;

use App\Models\Currency;
use Illuminate\Console\Command;

/**
 * Class RegisterCurrenciesCommand
 * @package App\Console\Commands\Manual
 */
class RegisterCurrenciesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'register:currencies {demo?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Register all needed currencies for project';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $questions = [
            'USD' => [
                'answer'    => $this->argument('demo') == true ? 'yes' : $this->ask('USD [yes|no]', 'yes'),
                'name'      => 'U.S dollars',
                'symbol'    => '$',
                'precision' => 2,
            ],
            'BTC' => [
                'answer'    => $this->argument('demo') == true ? 'yes' : $this->ask('BTC "bitcoin" [yes|no]', 'yes'),
                'name'      => 'Bitcoins',
                'symbol'    => '฿',
                'precision' => 8,
            ],


            'ETH' => [
                'answer'    => $this->argument('demo') == true ? 'yes' : $this->ask('ETH "Ether" [yes|no]', 'yes'),
                'name'      => 'Ether',
                'symbol'    => 'Ξ',
                'precision' => 8,
            ],

            'WEC' => [
                'answer'    => $this->argument('demo') == true ? 'yes' : $this->ask('Web Coin "WEC" [yes|no]', 'yes'),
                'name'      => 'WEC',
                'symbol'    => 'WEC',
                'precision' => 8,
            ],

            'ACC' => [
                'answer'    => $this->argument('demo') == true ? 'yes' : $this->ask('ACC "ACC" [yes|no]', 'yes'),
                'name'      => 'ACC',
                'symbol'    => 'ACC',
                'precision' => 8,
            ],
            'FST' => [
                'answer'    => $this->argument('demo') == true ? 'yes' : $this->ask('ACC "ACC" [yes|no]', 'yes'),
                'name'      => 'FST',
                'symbol'    => 'FST',
                'precision' => 8,
            ],
            'USDT' => [
                'answer'    => $this->argument('demo') == true ? 'yes' : $this->ask('"USDT" [yes|no]', 'yes'),
                'name'      => 'USDT',
                'symbol'    => 'USDT',
                'precision' => 8,
            ],

            'PZM' => [
                'answer'    => $this->argument('demo') == true ? 'yes' : $this->ask('Prizm "PZM" [yes|no]', 'yes'),
                'name'      => 'Prizm',
                'symbol'    => 'PZM',
                'precision' => 8,
            ],



        ];

        foreach ($questions as $currencyKey => $data) {
            $this->line('------');

            if ('yes' !== $data['answer']) {
                continue;
            }

            $this->info('Registering ' . $currencyKey);
            $checkExists = Currency::where('code', $currencyKey)->get()->count();

            if ($checkExists > 0) {
                $this->error($currencyKey . ' already registered.');
                continue;
            }

            $reg = Currency::create([
                'name'        => $data['name'],
                'code'        => $currencyKey,
                'symbol'      => $data['symbol'],
                'precision'   => $data['precision'],
                'currency_id' => $data['currency_id'] ?? null,
            ]);

            if (!$reg) {
                $this->error('Can not register ' . $currencyKey);
                continue;
            }

            $this->info($currencyKey . ' registered.');
        }
    }
}
