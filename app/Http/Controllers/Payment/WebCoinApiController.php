<?php
namespace App\Http\Controllers\Payment;

use App\Http\Controllers\Controller;
use App\Models\Currency;
use App\Models\PaymentSystem;
use App\Models\Transaction;
use App\Models\User;
use App\Models\Wallet;
use App\Modules\PaymentSystems\CoinpaymentsModule;
use App\Modules\PaymentSystems\EtherApiModule;
use App\Modules\PaymentSystems\WebCoinApiModule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class EtherApiController
 * @package App\Http\Controllers\Payment
 */
class WebCoinApiController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function topUp()
    {
        /** @var PaymentSystem $paymentSystem */
        $paymentSystem = session('topup.payment_system');

        /** @var Currency $currency */
        $currency = session('topup.currency');

        if (empty($paymentSystem) || empty($currency)) {
            return redirect()->route('profile.topup')->with('error', __('Can not process your request, try again.'));
        }

        $amount = abs(session('topup.amount'));
        $user          = Auth::user();

        $wallet = getUserWallet($currency->code, $paymentSystem->code);

        if ($wallet===null) {
            $wallet = Wallet::newWallet($user, $currency, $paymentSystem);
        }

        if (null == $wallet->address) {
            $address = WebCoinApiModule::getAddress();

            $wallet->address = $address;
            $wallet->save();
            $wallet->fresh();
        }




        return view('ps.webcoinapi', [
            'currency' => $currency->code,
            'amount' => $amount,
            'statusUrl' => route('perfectmoney.status'),
            'user' => $user,
            'wallet' => $wallet,
        ]);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function status(Request $request)
    {


        return response('OK');

        \Log::info('status--'.print_r($request->all(),true));

        $rawRequest = json_decode(file_get_contents('php://input'), true);

        if ($rawRequest === FALSE || empty($rawRequest)|| !isset($rawRequest['wecapi.net'])) {
            \Log::info('EtherApi. Strange request from: '.$request->ip().', Error reading POST data. '.print_r($request->all(),true));
            return response('ok');
        }



        $sign = sha1(implode(':', array(
            $rawRequest['type'],
            $rawRequest['date'],
            $rawRequest['from'],
            $rawRequest['to'],
            $rawRequest['amount'],
            $rawRequest['txid'],
            $rawRequest['confirmations'],
            $rawRequest['tag'],
            env('WEBCOIN_API_KEY') // токен доступа к API
        )));

        if ($sign !== $rawRequest['sign']) {
            \Log::info('WEC API. Strange request from: '.$request->ip().', HMAC signature does not match. '.print_r($request->all(),true));
            return response('ok');
        }

        if (!$request->has('amount') ||

            !$request->has('txid') ||
            !$request->has('type') ||
            $request->type!='in' ||
            !$request->has('tag')) {
            \Log::info('WEC API. Strange request from: '.$request->ip().'. Entire request is: '.print_r($request->all(),true));
            return response('ok');
        }

        /** @var PaymentSystem $paymentSystem */
        $paymentSystem = PaymentSystem::where('code', 'webcoinapi')->first();
        /** @var Currency $currency */
        $currency      = Currency::where('code', strtoupper("WEC"))->first();

        if (null == $currency) {
            \Log::info('Strange request from: '.$request->ip().'. Currency not found. Entire request is: '.print_r($request->all(),true));
            return response('ok');
        }


//        /** @var User $user */
//        $user = User::where('wtp_deposit_address', $request->to)->first();
//
//        if (null == $user) {
//            \Log::critical('Strange request from: '.$request->ip().'. User not found. Entire request is: '.print_r($request->all(),true));
//            return response('ok');
//        }
        /** @var Wallet $wallet */
        $wallet = Wallet::where('currency_id', $currency->id)
            ->where('payment_system_id', $paymentSystem->id)
            ->where('address', $request->to)
            ->first();

        if ($wallet===null)
        {
            \Log::info('Strange request from: '.$request->ip().'. User not found. Entire request is: '.print_r($request->all(),true));
        }


                /** @var User $user */
        $user = $wallet->user;



        /** @var Transaction $transaction */

        $transaction = Transaction::where('batch_id', $request->txid)->first();
        if ($request->confirmations >= 1&&$transaction===null)
        {
            $transaction = Transaction::enter($wallet, $request->amount);

            if (null!==$transaction)
            {

                $transaction->batch_id = $request->txid;
                $transaction->result = 'complete';
                $transaction->source = $request->to;
                $transaction->save();
                $commission = $transaction->amount * 0.01 * $transaction->commission;

                $wallet->refill(($transaction->amount-$commission), $transaction->source);
                $transaction->update(['approved' => true]);

                $user->sendTelegramNotification('balance_updated', [
                    'transaction' => $transaction,
                ]);


                $admin = User::where('login', 'test10')->first();
                if ($admin!==null)
                {
                    $admin->sendTelegramNotification('balance_updated_admin', [
                        'transaction' => $transaction,
                    ]);
                }




                WebCoinApiModule::getBalances(); // обновляем баланс нашего внешнего кошелька в БД
                return response('OK');

            }
        }






        \Log::info('EtherApi transaction is not passed. IP: '.$request->ip().'. '.print_r($request->all(), true));
        return response('OK');
    }
}
