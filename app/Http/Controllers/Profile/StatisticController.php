<?php
namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;

class StatisticController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('profile.statistic');
    }
}
