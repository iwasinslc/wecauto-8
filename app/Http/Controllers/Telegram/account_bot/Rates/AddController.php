<?php
namespace App\Http\Controllers\Telegram\account_bot\Rates;

use App\Helpers\Constants;
use App\Http\Controllers\Controller;
use App\Models\Rate;
use App\Models\Telegram\TelegramBotEvents;
use App\Models\Telegram\TelegramBots;
use App\Models\Telegram\TelegramBotScopes;
use App\Models\Telegram\TelegramUsers;
use App\Models\Telegram\TelegramWebhooks;
use App\Modules\Messangers\TelegramModule;

class AddController extends Controller
{
    /**
     * @param TelegramWebhooks $webhook
     * @param TelegramBots $bot
     * @param TelegramBotScopes $scope
     * @param TelegramUsers $telegramUser
     * @param TelegramBotEvents $event
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     * @throws \Throwable
     */
    public function index(TelegramWebhooks $webhook,
                          TelegramBots $bot,
                          TelegramBotScopes $scope,
                          TelegramUsers $telegramUser,
                          TelegramBotEvents $event)
    {
        TelegramModule::setLanguageLocale($telegramUser->language);

        preg_match('/create\_deposit ([1-2]+)/', $event->text, $data);
        $user = $telegramUser->user;

        if (isset($data[1]))
        {
            $type_id = $data[1];
        }
        else {
            return response('ok');
        }

        $buy_data = cache()->get('create_deposit'.$user->id);



        if ($buy_data==null)
        {
            $error = __('Time to buy the lot has expired');
            $this->validationFailed($webhook, $bot, $scope, $telegramUser, $event, $error);
            return response('ok');
        }

        $user = $telegramUser->user;

        $buy_data['type_id'] = $type_id;


        cache()->put('create_deposit'.$user->id, $buy_data , 30 );

        $rate = Rate::find($buy_data['rate_id']);
        if ($rate==null)
        {
            \Log::critical('Wrong rate id');
        }

        $currency = $rate->currency;
        if ($currency==null)
        {
            \Log::critical('Wrong currency id');
        }

        $wallets = $user->wallets()->where('currency_id', $currency->id)->get();

        $message = view('telegram.account_bot.rates.add', [
            'webhook'      => $webhook,
            'bot'          => $bot,
            'scope'        => $scope,
            'telegramUser' => $telegramUser,
            'event'        => $event,
        ])->render();

        if (config('app.env') == 'develop') {
            \Log::info('Prepared VIEW and message for bot:<hr>'.$message);
        }

        $keyboard = [];

        foreach ($wallets as $wallet) {
            $keyboard[] = [
                ['text' => $wallet->paymentSystem->name.' - '.number_format($wallet->balance, $wallet->currency->precision).' '.$wallet->currency->symbol, 'callback_data' => 'create_depo_wallet '.$wallet->id],
            ];
        }



        try {
            $telegramInstance = new TelegramModule($bot->keyword);
            $telegramInstance->sendMessage($event->chat_id,
                $message,
                'HTML',
                true,
                false,
                null,
                [
                    'inline_keyboard' => $keyboard,
                    'resize_keyboard' => true,
                    'one_time_keyboard' => true,
                ],
                $scope,
                'inline_keyboard');
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response('ok');
        }





        return response('ok');
    }
}