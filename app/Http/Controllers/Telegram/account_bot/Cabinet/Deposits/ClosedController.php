<?php
namespace App\Http\Controllers\Telegram\account_bot\Cabinet\Deposits;

use App\Http\Controllers\Controller;
use App\Models\Deposit;
use App\Models\Telegram\TelegramBotEvents;
use App\Models\Telegram\TelegramBots;
use App\Models\Telegram\TelegramBotScopes;
use App\Models\Telegram\TelegramUsers;
use App\Models\Telegram\TelegramWebhooks;
use App\Modules\Messangers\TelegramModule;

class ClosedController extends Controller
{
    /**
     * @param TelegramWebhooks $webhook
     * @param TelegramBots $bot
     * @param TelegramBotScopes $scope
     * @param TelegramUsers $telegramUser
     * @param TelegramBotEvents $event
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     * @throws \Throwable
     */
    public function index(TelegramWebhooks $webhook,
                          TelegramBots $bot,
                          TelegramBotScopes $scope,
                          TelegramUsers $telegramUser,
                          TelegramBotEvents $event)
    {
        TelegramModule::setLanguageLocale($telegramUser->language);

        $user = $telegramUser->user;

        $deposits = cache()->tags('userDeposits.' . $user->id)->remember('c.' . $user->id . '.userDeposits.active-' . 0, getCacheCLifetime('userDeposits'), function () use ($user) {
            $deposits = Deposit::where('user_id', $user->id)->where('active', 0);

            return $deposits->with([
                'transactions',
                'user',
                'rate',
                'wallet',
                'paymentSystem'
            ])->get();
        });

        if (count($deposits)==0)
        {
            $message = view('telegram.account_bot.deposits.havent', [
                'webhook'      => $webhook,
                'bot'          => $bot,
                'scope'        => $scope,
                'telegramUser' => $telegramUser,
                'event'        => $event,
            ])->render();

            if (config('app.env') == 'develop') {
                \Log::info('Prepared VIEW and message for bot:<hr>'.$message);
            }


            try {
                $telegramInstance = new TelegramModule($bot->keyword);
                $telegramInstance->sendMessage($event->chat_id,
                    $message,
                    'HTML',
                    true,
                    false,
                    null,
                    null,
                    $scope,
                    'inline_keyboard');
            } catch (\Exception $e) {
                \Log::error($e->getMessage());
                return response('ok');
            }
        }


        foreach ($deposits as $deposit)
        {
            $message = view('telegram.account_bot.deposits.deposit', [
                'webhook'      => $webhook,
                'bot'          => $bot,
                'scope'        => $scope,
                'telegramUser' => $telegramUser,
                'event'        => $event,
                'deposit'         => $deposit
            ])->render();

            if (config('app.env') == 'develop') {
                \Log::info('Prepared VIEW and message for bot:<hr>'.$message);
            }


            try {
                $telegramInstance = new TelegramModule($bot->keyword);
                $telegramInstance->sendMessage($event->chat_id,
                    $message,
                    'HTML',
                    true,
                    false,
                    null,
                    null,
                    $scope,
                    'inline_keyboard');
            } catch (\Exception $e) {
                \Log::error($e->getMessage());
                return response('ok');
            }
        }



        return response('ok');
    }
}