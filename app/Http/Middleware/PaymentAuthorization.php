<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class PaymentAuthorization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(!user()->isVerifiedEmail()) {
            return back()->with('error', __('Your email must be verified'));
        }

        if(
            $request->routeIs(['profile.transfer.store', 'profile.withdraw'])
            && !user()->isGoogle2FaEnabled()
        ) {
            return back()->with('error', __('2FA authentication must be enabled'));
        }

        return $next($request);
    }
}
