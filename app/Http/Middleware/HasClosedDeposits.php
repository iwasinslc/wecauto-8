<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class HasClosedDeposits
{
    /**
     * @param Request $request
     * @param Closure $next
     * @return \Illuminate\Http\RedirectResponse|mixed
     * @throws \Exception
     */
    public function handle(Request $request, Closure $next)
    {
        if (hasClosedDeposits()||user()->hasRole(['root'])) {
            return $next($request);
        }

        return redirect(route('profile.profile'))->with('error', __('You do not has any closed deposits'));
    }
}
